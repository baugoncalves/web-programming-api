package database

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

// SetConnection is a function to connect to database
func SetConnection() *sql.DB {
	driver := "mysql"
	user := os.Getenv("DATABASE_USER")
	server := os.Getenv("DATABASE_SERVER")
	password := os.Getenv("DATABASE_PASSWORD")
	port := os.Getenv("DATABASE_PORT")
	database := os.Getenv("DATABASE_DB")

	connectionDB, err := sql.Open(driver, user+":"+password+"@tcp("+server+":"+port+")/"+database)

	if err != nil {
		fmt.Println("Error to connect", err)
	}

	return connectionDB
}
