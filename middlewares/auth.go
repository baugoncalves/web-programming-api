package middlewares

import (
	"net/http"
	"strings"

	"gitlab.com/baugoncalves/web-programming-api/shared"
)

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var urlOrigin = r.URL.Path
		var routesToAnalyse []string = []string{"login"}
		var isToAnalyze = false

		urlOrigin = strings.Split(urlOrigin, "/")[1]

		for _, v := range routesToAnalyse {
			if v != urlOrigin {
				isToAnalyze = true
			}
		}

		if isToAnalyze {
			authHeader := strings.Split(r.Header.Get("Authorization"), "Bearer ")

			if len(authHeader) != 2 {
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte("Malformed Token"))
			} else {
				jwtToken := authHeader[1]

				token, err := shared.ValidateToken(jwtToken)

				if err != nil {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("Unauthorized"))
					return
				}

				if token.Valid {
					next.ServeHTTP(w, r)
				}
			}
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

/*func AuthMiddleware(next http.Handler) http.Handler {
	if len(share.APISecretKey) == 0 {
		log.Println("HTTP server unable to start, expected an APP_KEY for JWT auth")
	}
	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(share.APISecretKey), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
	})
	return jwtMiddleware.Handler(next)
}*/
