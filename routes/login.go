package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/baugoncalves/web-programming-api/shared"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Tokens struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	w.WriteHeader(http.StatusCreated)

	var user User
	var tokens Tokens

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}

	json.Unmarshal(body, &user)

	if user.Username == "amaury" && user.Password == "123456" {
		tokens.AccessToken = shared.GenerateJWToken("amaury", "access")
		tokens.RefreshToken = shared.GenerateJWToken("amaury", "refresh")
	}

	encoder.Encode(tokens)
}
