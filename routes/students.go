package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/baugoncalves/web-programming-api/database"
)

type Student struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

// GetStudents is a function to fetch all students
func GetStudents(w http.ResponseWriter, r *http.Request) {
	conn := database.SetConnection()
	defer conn.Close()
	var students []Student

	selDB, err := conn.Query("select *from students")

	if err != nil {
		fmt.Println("Error to fetch", err)
	}

	for selDB.Next() {
		var std Student

		err = selDB.Scan(&std.Name, &std.Email, &std.Phone, &std.ID)
		if err != nil {
			fmt.Println("Error to fetch", err)
		}
		students = append(students, std)
	}

	encoder := json.NewEncoder(w)
	encoder.Encode(students)
}

// NewStudent us a function to register a new student
func NewStudent(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	w.WriteHeader(http.StatusCreated)
	conn := database.SetConnection()
	defer conn.Close()

	var register Student

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}

	json.Unmarshal(body, &register)

	action, err := conn.Prepare("insert into students (name, email, phone) values(?,?,?)")
	if err != nil {
		fmt.Println(err)
	}

	res, err := action.Exec(register.Name, register.Email, register.Phone)
	if err != nil {
		fmt.Println(err)
	}

	lastID, err := res.LastInsertId()
	studentID := int(lastID)
	register.ID = studentID

	encoder.Encode(register)
}

// GetStudentByID is a function to featch a student by id
func GetStudentByID(w http.ResponseWriter, r *http.Request) {
	conn := database.SetConnection()
	defer conn.Close()
	var std Student

	vars := mux.Vars(r)
	id := vars["studentID"]

	selDB := conn.QueryRow("select *from students where id=" + id)

	err := selDB.Scan(&std.Name, &std.Email, &std.Phone, &std.ID)
	if err != nil {
		fmt.Println(err)
	}

	encoder := json.NewEncoder(w)
	encoder.Encode(std)
}

// UpdateStudent us a function to update a student
func UpdateStudent(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	w.WriteHeader(http.StatusCreated)
	conn := database.SetConnection()
	defer conn.Close()

	var register Student

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}
	json.Unmarshal(body, &register)

	vars := mux.Vars(r)
	id := vars["studentID"]

	action, err := conn.Prepare("update students set name=?, email=?, phone=? where id=?")
	if err != nil {
		fmt.Println(err)
	}

	_, err = action.Exec(register.Name, register.Email, register.Phone, id)
	if err != nil {
		fmt.Println(err)
	}
	encoder.Encode("Student updated with success")
}

// DeleteStudent us a function to delete a student
func DeleteStudent(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	w.WriteHeader(http.StatusCreated)
	conn := database.SetConnection()
	defer conn.Close()

	vars := mux.Vars(r)
	id := vars["studentID"]

	action, err := conn.Prepare("delete from students where id=?")
	if err != nil {
		fmt.Println(err)
	}

	_, err = action.Exec(id)
	if err != nil {
		fmt.Println(err)
	}
	encoder.Encode("Student deleted with success")
}
