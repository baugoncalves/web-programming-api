package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/baugoncalves/web-programming-api/middlewares"
	"gitlab.com/baugoncalves/web-programming-api/routes"
)

func majorRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to Web Programming API")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", majorRoute)
	router.HandleFunc("/students", routes.GetStudents).Methods("GET")
	router.HandleFunc("/students/{studentID}", routes.GetStudentByID).Methods("GET")
	router.HandleFunc("/students", routes.NewStudent).Methods("POST")
	router.HandleFunc("/students/{studentID}", routes.UpdateStudent).Methods("PUT")
	router.HandleFunc("/students/{studentID}", routes.DeleteStudent).Methods("DELETE")
	router.HandleFunc("/login", routes.Login).Methods("POST")
}

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	var router *mux.Router

	log.Printf("Server is working on http://localhost:1688")

	router = mux.NewRouter()

	router.Use(middlewares.JsonMiddleware)
	router.Use(middlewares.AuthMiddleware)

	setRoutes(router)

	header := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization", "Origin"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"})
	origins := handlers.AllowedOrigins([]string{"*"})

	err := http.ListenAndServe(":1688", handlers.CORS(header, methods, origins)(router))
	if err != nil {
		fmt.Println("Error", err)
	}
}
