package shared

import (
	"fmt"
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// GenerateJWToken is a function that generates a token
func GenerateJWToken(user, typeToken string) string {
	tokenTime := time.Minute * time.Duration(15)
	if typeToken == "refresh" {
		tokenTime = time.Hour * 24 * 15
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user,
		"exp":      time.Now().Add(tokenTime).Unix(),
		"iat":      time.Now().Unix(),
	})
	tokenString, err := token.SignedString([]byte(APISecretKey))
	if err != nil {
		/*w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, `{"error":"token_generation_failed"}`)*/
		log.Println("error token_generation_failed")
		return ""
	}

	return tokenString
}

// ValidateToken is a function that valids a token
func ValidateToken(tokenStr string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte(APISecretKey), nil
	})

	return token, err
}

// ParseToken is a function that valids a token and extracts an user
func ParseToken(tokenStr string) (string, bool) {
	token, err := ValidateToken(tokenStr)

	if err != nil {
		return "", false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		iduser := claims["username"].(string)
		return iduser, true
	}
	return "", false
}

// RefreshToken is a function that renews a token
func RefreshToken(refreshTokenStr string) ([]string, error) {
	var tokens []string

	username, worked := ParseToken(refreshTokenStr)

	if !worked {
		return tokens, fmt.Errorf("Invalid token")
	}

	accessToken := GenerateJWToken(username, "access")
	refreshToken := GenerateJWToken(username, "refresh")

	tokens = append(tokens, accessToken)
	tokens = append(tokens, refreshToken)

	return tokens, nil
}
